ansible-workstation
===================

[Ansible] playbooks to set up my workstations, laptops, etc.

`$ ansible-pull --url https://gitlab.com/outputenable/ansible-workstation.git --ask-become-pass --extra-vars @$HOME/ansible_vars.yml [<playbook.yml>]`

[Ansible]: https://www.ansible.com
